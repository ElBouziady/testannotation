#include "mainwindow.h"
#include <QApplication>


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <iostream>
#include <stdio.h>
#include <QFile>
#include <QTextStream>
#include <fstream>
using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    Mat src,crop;
    cv::Rect roi_b;
    QStringList list;
    stringstream ssfn;
    int filenumber=0; // Number of file to be saved
    string filename;
    QString chemin;
    QFile inputFile("/home/boukary/Desktop/predictedLocations.txt");
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            list=line.split(" ");
            src=imread(list.at(0).toStdString().c_str());
            if(list.at(1).toInt()==0)
                continue;

            cout<<list.size()<<endl;
            for(int i=2;i<list.size();i+=4)
            {
                roi_b=Rect(list.at(i).toInt(),list.at(i+1).toInt(),list.at(i+2).toInt(),list.at(i+3).toInt());
                crop = src(roi_b);



                filename = "/home/boukary/Pictures/hopital_crop/";

                ssfn <<filename<< filenumber << ".bmp";
                filename = ssfn.str();
                filenumber++;

                chemin=ssfn.str().c_str();
                imwrite(ssfn.str(), crop);
                ssfn.str("");
                rectangle(src,Rect(list.at(i).toInt(),list.at(i+1).toInt(),list.at(i+2).toInt(),list.at(i+3).toInt()),Scalar(255,0,0));


                //putText(src,list.at(0).toStdString().c_str(),cvPoint(24, 24),FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0, 0, 255), 1, CV_AA);


            }

            /*if(!src.empty()){
                cout<<"here"<<endl;
                    imshow("window", src);
                }
*/

            imshow("image",src);
            waitKey(0);
        }

        inputFile.close();
    }



    return a.exec();
}
